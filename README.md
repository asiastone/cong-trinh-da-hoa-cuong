# Công trình đá hoa cương

AsiaStone nhận thi công ốp lát các loại đá hoa cương thiên nhiên, marble với nhiều hạng mục thi công như:

- Ốp đá hoa cương mặt tiền

- [ốp đá mặt tiền biệt thự](https://asiastone.vn/op-da-mat-tien-nha-pho-nha-ong)

- lát đá phòng khách

- thi công đá mặt bếp

- [lát đá cầu thang](https://asiastone.vn/op-lat-da-hoa-cuong-cau-thang)

- ốp đá bậc tam cấp

- ốp đá granite thang máy

- [cắt đá hoa cương theo yêu cầu](https://asiastone.vn/cat-da-hoa-cuong-mat-ban-bep)

Nhận báo giá tại đây: đá granite lát nền, đá hoa cương marble